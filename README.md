# Urban Regeneration in Kyoto

> WPI B-Term 2018 Interactive Qualifying Project in Kyoto, Japan

> Completed by Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant

> Approved by Professor Jennifer deWinter

This Git repository contains the source code for the Electronic Field Form for Urban Revitalization (EFFURT), in addition to the research and final deliverables (including presentations and reports) that were used to fulfill the completion of the accompanying Interactive Qualifying Project (IQP).

## Overview of Repository Contents

### Research Materials

The `assets/` and `proposal/` folder contains the accumulated research materials from this IQP. These include images, articles, preliminary reports produced by team members, and infographics produced by team members. 

The `assets/` folder is symbolically linked ("symlinked") to from the `proposal/`, `report/`, `presentations/`, and `src/` folders in order to facilitate automatic building of various project assets on Unix (Linux and macOS) systems.

### Presentations

The `presentations/` folder contains every presentation given by team members to project stakeholders. These presentations are written using [Markdown](https://en.wikipedia.org/wiki/Markdown) files, and built by a tool called [Backslide](https://github.com/sinedied/backslide). 

### Report

The `report/` folder contains the final report produced by this IQP for stakeholders. Each section of the report is contained in a separate [Markdown](https://en.wikipedia.org/wiki/Markdown) file, and all of the files are compiled into a styled PDF document at build time using a tool called [ReLaXed](https://github.com/RelaxedJS/ReLaXed).

### EFFURT - TODO

The `src/` folder contains the source code for the Electronic Field Form for Urban Regeneration (EFFURT). 

EFFURT is built as a single-page web application and is designed to be served as static content from an HTTP(S) web server. In addition to JavaScript, HTML5, and CSS, EFFURT uses the following core technologies:

1. [Leaflet](TODO) for rendering and manipulating map tiles, layers, and markers. Leaflet was chosen over alternatives like Mapbox and Google Maps due to its open source license and large amount of documentation.

2. [PouchDB](TODO) for storing information about urban assets in the web browser's local storage and synchronizing with a remote [CouchDB](TODO) instance.

3. [Rivets.js](TODO) for binding HTML5 views (e.g., forms, modals, etc.) to JavaScript models. Rivets.js was chosen over alternatives like Angular.js and Vue.js for its simplicity and unopinionated syntax, which made it highly compatible with both Leaflet and PouchDB.

While EFFURT uses a remote CouchDB database to synchronize data between different web browsers and users, EFFURT is capable of running without this database present. In the absence of a database, users will only be able to see and modify the urban assets they create on their web browsers, and not information from other browsers.

## Building the Repository Contents

All of the components of this IQP (proposals, presentations, reports, and EFFURT) are built via [Node.js](https://nodejs.org). To build this IQP, download and install Node.js from [here](https://nodejs.org/en/download/) and then execute the following command within a command prompt within this project's root directory:

    npm run build

Upon successful completion, all PDF files and compiled EFFURT source code will be available in the `_site/` folder. This folder can be deployed directly to an HTTP server for easy access over the web, or accessed directly from your computer by opening the `index.html` file within the `_site/` folder in your preferred web browser.

### Modifying the Build Process

The entire build process is managed via the Node Package Manager (`npm`) and a JavaScript build file in the root of this repository called `build.js`. To modify or see how the build process works, take a look inside of the [build.js file](build.js).

## A Note on Markdown
Most of the content in this repository (excluding EFFURT source code) is written using [Markdown](https://en.wikipedia.org/wiki/Markdown).

Essentially, Markdown is a way of writing in plain-text (kind of like programming) in such a way that that text can be converted into a beautiful document in various formats. This file (`README.md`) is also written in Markdown. Here are some basic tips which cover 90% of our needs with Markdown:

1. Headers start with `#`. The more `#`, the lower level the header is; `# This is an H1`, `## This is an H2`, `### This is an H3`, and so on.

2. Italicized text is surrounded by *single* asterisks `*like this*`.

3. Bold text is surrounded by **double** asterisks `**like this**`.

4. Italicized bold text is surrounded by ***triple*** asterisks `***like this***`.

5. Links are inserted by using a combination of square brackets (the link text shown to the reader) and parentheses (the actual link). For example, the link to the Markdown wikipedia article above in Markdown syntax looks like `[Markdown](https://en.wikipedia.org/wiki/Markdown)`.

6. Images are inserted just like links, but with an exclamation point before them. The text within the square brackets is used as "placeholder" text, and is a brief description of the image. For example, if I wanted to show a picture of Perry the Platypus, the Markdown syntax looks like `![Perry the Platypus](https://upload.wikimedia.org/wikipedia/en/d/dc/Perry_the_Platypus.png)`.

7. Numbered lists (like this one) are just a series of lines that start with numbers.

8. Bulleted lists are just a series of lines that start with dashes (`-`).