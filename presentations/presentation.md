class: animation-fade
layout: true

.bottom-bar[
  Regenerating Yokai Street
]

---
class: impact, overlay
background-image: url(assets/urk-summary.jpg)

# Yokai Street
## A Historic Shotengai in a Modern World

### .pop[妖怪]ストリート: .pop[現代]世界に.pop[歴史的]商店街

.small.thin[Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant]

---

.col-6[
.pic-full-left[![](assets/urk-summary.jpg)]
]

.col-6[
# Introduction
## Taishogun
.big[
1. **History**
2. Decay
3. Regeneration
]
]

.bottom-bar[
**序文** Introduction
]

---

.col-6[
.pic-full-left[![](assets/yokai-street.jpg)]
]

.col-6[
# Introduction
## Yokai Street
.big[
1. History
2. **Decay**
3. Regeneration
]
]

.bottom-bar[
**序文** Introduction
]

---
class: prop

.col-6[
.pic-full-left[![](assets/yokai-street-bleak.jpg)]
]

.col-6[
# Introduction
## Our Project
.big[
1. History
2. Decay
3. **Regeneration**
]
]

.bottom-bar[
**序文** Introduction
]

---
class: impact, overlay
background-image: url(assets/yokai_map_landscape.jpg)

## Research and Analysis

### .pop[研究]と.pop[分析]

.bottom-bar[
**研究と分析** Research and Analysis
]

---

# .small[Infrastructure: Mapping Assets with .pop[EFFURT]]

<iframe src="https://urban-regen.alicorn.io/effurt" style="position: absolute; top: 0; left: 0; display:block; width: 100%; height: 93%; z-index: 0;" frameborder="0"></iframe>

.bottom-bar[
**研究と分析** Research and Analysis | The Electronic Field Form for Urban Regeneration (EFFURT)
]

---

# Tourism: The .pop[Market]

.row.center-v[
.col-8[
.responsive-height[![](assets/figures/tourist-interest-summary.png)]   
]

.col-4.cul[
- 30 responses
- 29 interested in Yokai-inspired culture
- 18 interested in Yokai specifically
]
]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
class: prop

# Tourism: Our .pop[Visibility]

.responsive-height[![](assets/figures/tourist-website-summary.png)]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
.col-6[
.pic-full-left[![](assets/yokai-collage.jpg)]
]

.col-6[
# Tourism
## Our Strengths
.big[
.cul[
- Yokai Brand
- Location
- History
]
]
]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
class: prop

.col-6[
.pic-full-left[![](assets/yokai-not-for-sale.jpg)]
]

.col-6[
# Tourism
## The Opportunities
.big[
.cul[
- **Integration**
- Continuity
- Visibility
]
]
]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
class: prop

.col-6[
.pic-full-left[![](assets/yokai-street-unimagined.jpg)]
]

.col-6[
# Tourism
## The Opportunities
.big[
.cul[
- Integration
- **Continuity**
- Visibility
]
]
]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
class: prop

.col-6[
.pic-full-left[![](assets/yokai-street-map-point.jpg)]
]

.col-6[
# Tourism
## The Opportunities
.big[
.cul[
- Integration
- Continuity
- **Visibility**
]
]
]

.bottom-bar[
**研究と分析** Research and Analysis
]

---
class: impact, overlay
background-image: url(assets/yokai-tin.jpg)

## Recommendations

### .pop[推奨]

---

.col-6[
.pic-full-left[![](assets/yokai-tin.jpg)]
]

.col-6[
# Integration
## Sell the Yokai Brand

.big.cul[
- Brands **≠** Themes
- People *buy* brands *through* **merchandise**
]
]

.bottom-bar[
**推奨** Recommendation to Improve Integration
]

---

.col-6[
.pic-full-left[![](assets/yokai-mob.jpg)]
]

.col-6[
# Integration
## Start a Yokai Hunt

.big.cul[
- QR Codes
- **LINE** Stickers
- Yokai History
]
]

.bottom-bar[
**推奨** Recommendation to Improve Integration
]

---

.col-6[
.pic-full-left[![](assets/yokai-street-unimagined.jpg)]
]

.col-6[
# Continuity
## Invest in Spaces

.big.cul[
- Green space
- Obvious entrance
]
]

.bottom-bar[
**推奨** Recommendation to Improve Continuity
]

---

.col-6[
.pic-full-left[![](assets/yokai-street-unimagined.jpg)]
]

.col-6[
.pic-full-right[![](assets/yokai-street-reimagined.jpg)]
]

.bottom-bar[
**推奨** Recommendation to Improve Continuity
]

---

.col-6[
.pic-full-left[![](assets/yokai_map_landscape.jpg)]
]

.col-6[
# Visibility
## Refresh the Map

.big.cul[
- More Languages
- More Information
- Simpler Layout
]
]

.bottom-bar[
**推奨** Recommendation to Improve Visibility
]

---
.responsive-width[![](assets/tourist_map_v2.jpg)]

.bottom-bar[
**推奨** Recommendation to Improve Visibility
]

---

.row.center-v[
.col-6[
.responsive-width.pic[![](assets/kyoto-emblem.png)]
]

.col-6[
# Visibility
## Incentivize Membership

.big.cul[
- Grant **assistance**
- Brand **guidance**
- Local **advocacy**
]
]
]

.bottom-bar[
**推奨** Recommendation for Visibility
]

---
class: impact

.row.center-v[
.col-6[
# Q&A

Email: [gr-urbanregenb18@wpi.edu](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
]

.col-6[
.responsive-width.pic[![](assets/tourist_map_v2.jpg)]
]
]