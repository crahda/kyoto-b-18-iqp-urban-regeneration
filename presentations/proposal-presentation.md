class: animation-fade
layout: true

.bottom-bar[
  Regenerating Yokai Street
]

---
class: impact

# Regenerating Yokai Street
Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Our .pop[Goals]

.row.center-v[
.col-6[
.responsive.pic[![](assets/yokai-soho-nd.jpg)]
]

.col-6.cul[
1. **Survey** shōtengai
2. **Promote** Yokai Street
3. **Visualize** factors of decay
]
]

.bottom-bar[
Image: https://yokaisoho.com/building/ | Background Image: Albert Dera via [Unsplash](https://unsplash.com/photos/3A4YNbpn9Sc)
]

???
- Survey shōtengai in the Kamigyo ward for factors of urban decay.
- Visualize factors of urban decay and renewal in shōtengai.
- Promote Yokai Street in its community and raise visibility for tourists.

---
class: overlay
background-image: url(assets/tomasso-nd.jpg)

# Global Factors of Urban .pop[Decay]

Urban Decay | Poor Urban<br>Planning | Departing<br>Businesses | Mismanaged<br>Public Space
---|---|---|---
Johannesburg | | ✔ | ✔
Santiago | ✔ | | ✔
Seoul | ✔ | ✔ | ✔
Buenos Aires | ✔ | ✔ | 

.bottom-bar[
Background Image: Patrick Tomasso via [Unsplash](https://unsplash.com/photos/uThLDG9Y4ms)
]

???

The depreciation of a city or section of a city which leads to collapsed buildings, abandoned property, more crime, and unemployment.

background-image: url(assets/hendy-2014-yubari.jpg)
.alt[Image: *Hendy, R. via The Guardian*]

---
class: overlay
background-image: url(assets/lukas-nd.jpg)

# Global Factors of Urban .pop[Renewal]

Urban Renewal | Urban<br>Replanning | Government<br>Committees | Small Business<br>Investments
---|---|---|---
Johannesburg | ✔ | ✔ |
Santiago | | ✔ | ✔
Seoul | ✔ | ✔ |
Buenos Aires | ✔ | ✔ | ✔

.bottom-bar[
Background Image: Lukas via [Unsplash](https://unsplash.com/photos/80UpjrtFxLA)
]

---

# Urban Decay in .pop[Japan]

.row.center-v[
.col-6[
.responsive-width.pic[![](assets/kamouni-2016-yubari.jpg)]   
]
.col-6[
.responsive-width.pic[![](assets/crossley-baxter-2018-homes.jpg)]
]
]

.bottom-bar[
Left: *Kamouni, S. via The Sun* | Right: *Crossley-Baxter, L. via Rethink Tokyo*
]

---

# Factors of Decay in .pop[Shōtengai]

.responsive-height.center-h[![](assets/team-data-via-horioka-2006-2.png)]

.bottom-bar[
Data: *Horioka, C.Y. via Japan and World Economy*
]

???
Image: *Ito, M. via Japan Times*
Data: *Horioka, C.Y. via Japan and World Economy*

- Rapid growth in the 1980s
- Economic burst in 1991
- Consolidation into department stores

***TODO: Infographic that shows data, and annotates as GROWTH, BURST, CONSOLIDATION***

---

# The .pop[Yokai Street] Shōtengai

.row.center-v[
.col-6[
.responsive.pic[![](assets/arnold-nd.jpg)]
]
.col-6[
.responsive.pic[![](assets/yokai-street-web-1.png)]
]
]

.bottom-bar[
Left: *Arnold, J. via Atlas Obscura* | Right: http://kyoto-taisyogun.com/en/
]

???
- A shotengai in Kamigyo Ward that serves elderly community members.
- Launched and abandoned an English website in 2015.

.responsive.pic[![](assets/arnold-nd.jpg)]
Left: *Arnold, J. via Atlas Obscura*

---

# .pop[Mapping] Out Decay

.row.center-v[
.col-6[
.responsive.pic[![](assets/lseps-2018-2.png)]
]
.col-6[
.responsive.pic[![](assets/social-value-lab-2012.jpg)]
]
]

.bottom-bar[
Left: https://booth.lse.ac.uk/map | Right: http://www.socialvaluelab.org.uk
]

???

.col-12[
.responsive-width-sm.pic[![](assets/lseps-2018.jpg)]
]

.col-12[
> Image: *London School of Economics and Political Science, 2018.*
]

---

# .pop[Urban Asset] Mapping


<iframe src="https://communitymaps.org.uk/project/thames-ward?center=51.5194:0.1114:13" style="position: absolute; top: 0; left: 0; display:block; width: 115%; height: 100%;" frameborder="0"></iframe>

.bottom-bar[
Data via https://communitymaps.org.uk/project/thames-ward
]

???
- https://communitymaps.org.uk/project/thames-ward
- Interactively visualize urban assets
- Composed from multi-modal data
- Actionable information for stakeholders

.row.center-v[
.col-6[
.responsive-width[![](assets/social-value-lab-2012.jpg)]
]
.col-6[
.responsive-width[![](assets/heinricher-kahn-maitland-manor-2013.png)]
]
]

.row.center-v[
.col-6[
> Image: *Social Value Lab, 2012.*   
]

.col-6[
> Image: *Venice Bells IQP*
]
]

---

# Our .pop[Plan]

.responsive-height.center-h[![](assets/the-plan.png)]

???

1. What assets, demographics, and events are nearby?

2. What shotengai are similar and dissimilar?

3. What physical signs of decay are there?

---

## .pop[EFFURT]<br>.smol-h[The Electronic Field Form for Urban RegeneraTion]

.row.center-v[
.col-7[
.responsive.pic[![Urban Map](assets/urban-map.png)]
]

.col-5.cul[
- Built with open-source technology
- Works online and offline
]
]

???
**TODO:** Add drop shadow around picture

---

# Phase .pop[I]: Regional Survey

.row.center-v[
.col-6.cul[
.responsive.pic[![Urban Map](assets/overpass-turbo-tourism-2018.PNG)]
]

.col-6.cul[
- Geographic information systems
- Government census data
- Cultural centers and tourism agencies
]
]

.bottom-bar [
Data via: http://overpass-turbo.eu/
]

???

- **Extract** big-data from GIS
- **Interpret** census data
- **Search** for regional events and activities

---

# Phase .pop[II]: Shōtengai Categorization

.row.center-v[
.col-6.cul[
.responsive.pic[![Urban Ley-Lines](assets/ley-lines.png)]   
]

.col-6.col[
1. **Visualize** urban anchors
2. **Identify** urban ley-lines
3. **Categorize** shōtengai
]
]

---

# Phase .pop[III]: Local Surveys

.row.center-v[
.col-6[
.responsive.pic[![OSM and Overpass](assets/overpass-turbo-shop-2018.PNG)]
]

.col-6.cul[
- **Discover** new urban assets
- **Fact-check** GIS data
- **Record** factors of urban decay and success
]
]

.bottom-bar [
Data via: http://overpass-turbo.eu/
]

---

## 1st Deliverable: Yokai Street **Tourism Map**

.responsive-height.center-h[![](assets/tourist-map-mock.png)]

---
class: impact

# Thank You!
Email us any time at: [gr-kyoto-b18-urbanregen@wpi.edu](mailto:gr-kyoto-b18-urbanregen@wpi.edu)