This report is the result of a Worcester Polytechnic Institute (WPI) Interactive Qualifying Project (IQP) in Kyoto, Japan. This project was jointly sponsored by WPI and Benoit Jacquet, a member of the Yokai Street shop owner's association.

![The team would like to thank Leslie Knope for being our spirit animal throughout this journey.](assets/gr-urbanregen-b18.jpg)