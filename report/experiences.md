# Experiences
Modern tourists actively seek out new "experiences", so it follows that enriching Yokai Street with experiences would lead to greater tourism. In this section, we cover the following topics:

- The Definition of an Experience

- A Form for Identifying and Assessing Experiences

## Experiences and Yokai Street

Traditionally, businesses in commercial districts offer commodities, goods, or services. In the traditional shotengai, produce shops sell produce; clothing shops sell clothes; restaurants serve food, and so on. An experience is something distinct from products and services—it offers a memorable experience for the consumer (Pine & Gilmore, 1998). The production of an experience for the consumer drives consumers to pay more for a service that could be obtained elsewhere—it’s the reason customers are willing to pay exponentially higher prices at Tokyo’s Aragawa Steakhouse for the same service they could receive at an Outback Steakhouse in the United States (Pizam, 2010).

Tourists, and especially young tourists actively seek out experiences. As a tourist destination, Japan offers many different experiences, from owl cafes to traditional geisha tea ceremonies. Kyoto is home to many of these unique experiences, including Fire Ramen-a restaurant where the ramen is lit on fire, a hedgehog cafe, Toei Studio Park, and a Starbucks hidden inside a traditional tea house in the district of Gion. 

Research has shown that the millennial generation is the driving force behind this “experience economy” that has erupted both globally and in Japan (Lewis & Jacobs, 2018, Cataldo, 2018, Kohler, 2017). Millennials are more likely to spend money on experiences rather than desirable objects. (Lewis & Jacobs, 2018, Kohler, 2017). Based upon these findings, we believe that Yokai Street will be more likely to attract young visitors and tourists if it offers unique experiences that cannot be found elsewhere.

Revitalization projects centered around unique activities and experiences may also be more likely to attract involvement of local youth and entrepreneurs. Due to Japan’s aging population one of the challenges shared by many shotengai, including Yokai Street, is that many shop owners are aging without successors to take over their shop when they reach retirement (METI, 2014; Brumann & Shulz, 2012). Without successors or young entrepreneurs to take over storefronts, shops gradually become vacant and the shotengai may collapse, as did a once bustling shotengai in the Izumisano shopping district of Osaka prefecture (Nashima, 1997). Yokai Street currently has several vacant storefronts, and the at least one shop has closed in the past year alone. 

We examined case studies of 30 shotengai that have undertaken revitalization projects in the past decade. Fifteen of these shotengai cited vacant shops as an issue on their street, and four cited aging shop owners and lack of successors as a contributing factor to vacant storefronts. All of these studies said getting young people involved with the shotengai was important to revitalization. Many shopping association representatives said involving young entrepreneurs and community members brought more life and energy into the shopping association and the district as a whole (METI, 2014). 

It is clear that a balance of young people in a district is vital to the health of a shotengai. If the Yokai Street district wants to continue to survive, more young people need to become involved. Adding exciting new experiences and activities to the street may be one strategy to increase visitors, traffic, and involvement of young shop owners and community members. 


## The Definition of an Experience

In order to identify existing experiences and create new experiences in Yokai Street, we needed to understand and define what constitutes an experience. To do this, our team scoured online websites, studies, and projects related to experience tourism, and found that experiences are built from four major components:

1. **Entertainment**: Does the visitor enjoy the experience by *watching* or *observing* it?

2. **Education**: Does the visitor *learn* something from the experience?

3. **Esthetic**: Does the visitor enjoy the beauty or the esthetic of the experience itself?

4. **Escapism**: Does the visitor enjoy the experience by *participating* in it?

Many experiences have one or two of these components, but the ideal experience emphasizes all of these components simultaneously. (Pine, J. & Gilmore, J. 1998 and Pizam, A. 2010)

Because it would be difficult to simultaneously use all four of these components when attempting to explain to other groups what an experience is, our team created a new definition of experience that applies to *any experience designed for-profit*:

> An economic offering that evokes an emotional response from consumers, creating a unique discourse community and personal memories.

## A Form for Identifying and Assessing Experiences

To streamline the identification and assessment of experiences in, around, and near Yokai Street, our team created an electronic survey form to act as a "framework" for analyzing experiences.

This form, which can be viewed in detail in Appendix **TODO**, is designed to answer the following key questions:

- How do people perceive the experience *before* experiencing it?
- What are the critical components of the experience that make it succeed or fail?
- How can the experience be improved?
- How satisfactory is the experience?
- Is the experience a "one-and-done" experience, or is it repeatable?