# Urban Assets: Regeneration through Development

The applications of maps extend beyond their ability to represent the criss-crossing roads of cities: Maps can display points of interest, convey geospatial statistics, and even depict the relationships between objects. Their diverse utility makes maps an ideal medium for simultaneously visualizing the current state of Yokai Street and creating promotional materials to increase the number of tourists that visit the street.

To visualize the current state of Yokai Street and its surrounding area, we created a custom mapping tool called the Electronic Field Form for Urban RegeneraTion (**EFFURT**). In this section, we briefly review some of the background research that led to the creation of EFFURT before diving into what EFFURT is, how we used it to gather data about Yokai Street, and what recommendations we were able to make based on our data gathered from EFFURT.

## Urban Asset Mapping

While many kinds of tools and techniques for creating maps exist, our team was particularly interested in a specific form of mapping called Urban Asset Mapping. As its name implies, Urban Asset Mapping visualizes the different assets within an urban setting via interactive multi-modal maps. 

After an extensive review of existing literature, our team was not able to find a complete definition of what constitutes an urban asset. As a result, we compiled information from various sources (Social Value Lab, 2012, OPEN GLasgow, 2014, South Seeds, 2017, and general tourism across Japan) to create a partial list of typical assets: 

- Transportation (e.g., parking, bus stops, train stations, taxi stands) 
- Healthcare (e.g., clinics, hospitals, pharmacies) 
- Education (e.g., libraries, schools, universities) 
- Public Spaces (e.g., parks, gardens) 
- Recreation (e.g., network cafes, gymnasiums, theaters) 
- Food (e.g., grocery stores, restaurants, convenience stores) 
- Religion (e.g., churches, temples, shrines) 
- Infrastructure (e.g., government offices, post offices) 
- Investment (e.g., empty plots of land, abandoned buildings)
- Property (e.g., homes, apartments)
- Service (e.g., dry cleaning, hair salon, mechanic)

The mapping of Yokai Street focused on identifying and cataloguing these assets, with a particular focus on assets for tourism and commercial development: *transportation, public spaces, recreation, food, historical assets, investment,* and *services*. In order to map these assets, we reviewed different methods of urban asset mapping used for revitalization worldwide.

![Fig. 1.1: One composite asset map produced by Social Value Lab. (Social Value Lab, 2012)](assets/social-value-lab-2012.jpg)

A prominent example of Urban Asset Mapping can be seen in a report on the Scottish town of Maybole published by the Social Value Lab in 2012. Figure 1.1 shows one of the composite assets maps produced as a result of the Social Value Lab's survey of the Scottish town of Maybole. Blue dots indicate libraries, orange dots indicate educational assets, light green dots indicate leisure assets, and dark green dots indicate transportation assets. We believe it is important to note that some of the maps in the Social Value Lab's full report used a combination of asset mapping and on-site surveys to describe issues with accessing or utilizing assets; an Urban Asset Map is, therefore, not just a map of physical Urban Assets. The Social Value Lab was able to make extensive use of these composite asset maps (Fig. 1.1) --- generated in part from community surveys --- to create a detailed list of recommendations to improve the community's health and safety. (Social Value Lab, 2012)

![Fig. 1.2: An interactive map of prominent bell towers in Venice. (Heinricher, Kahn, Maitland, and Manor, 2013)](assets/heinricher-kahn-maitland-manor-2013.png)

A more recent example of Urban Asset Mapping can be seen in a 2013 Worcester Polytechnic Institute (WPI) Interactive Qualifying Project (IQP) which mapped bell towers across Venice (Heinricher, Kahn, Maitland, and Manor, 2013) (Fig. 1.2). Some of the bell towers are clickable, allowing users to hear the sound of the bell and see a picture of the tower. The different colors of the towers indicate how much data was collected for each tower. They leveraged open-source mapping technology to produce an interactive map of Venice's bell towers which can be used by members of the public to learn more about local history. 

<div style="page-break-after: always;"></div>

## Yokai Street Urban Asset Mapping with EFFURT

![Fig. 1.3: A general view of the EFFURT application.](assets/effurt-overview-2.png)

To ensure the data gathered during our research of the area around Yokai Street could be normalized, stored, and visualized across multiple digital and physical mediums, our team created a novel mapping tool called the Electronic Field Form for Urban RegeneraTion (**EFFURT**). The above figure is the view of EFFURT containing the created assets. (Fig. 1.3)

Our team created EFFURT because there were few (if any) existing tools that would allow our team to quickly collect, store, and visualize data for urban assets without paying exorbitant sums for a professional Geographic Information System (GIS). 

<div style="page-break-after: always;"></div>

#### Features

![Fig. 1.4: View of the EFFURT application where the user is viewing information about an urban asset (left) and editing the information for a specific urban asset (right).](assets/effurt-editing-asset.png)

EFFURT was designed to support the collection of information, viewing of collected information, and viewing of publicly available data. These features, detailed below, allow users to easily **survey**, **collaborate**, **visualize**, and **analyze** urban assets.

- **Urban Asset Surveying:** Users can quickly conduct on-the-ground surveys and create new urban assets (Fig. 1.5) within EFFURT.

- **Collaborative Editing:** EFFURT uses a robust, distributed data storage system that allows surveyors to gather data on mobile devices while offline, and then synchronize that data with one another while online. This allows users to easily create, share, and edit existing urban assets, as seen on the right of figure 1.4.

- **Urban Asset Visualization and Analysis:** Users can view detailed information about urban assets based on our survey findings and background research, as seen on the left of figure 1.4. This includes information like the specific urban assets offered at a given urban asset, imagery of the asset, and brief analysis of the state of the asset.

- **Public Dataset Integration:** EFFURT integrates with external GIS services to display topographic, satellite, and street map data, in addition to urban asset information provided by external services like Overpass.

![Fig. 1.5: View of the EFFURT application where the user is editing the location of existing urban assets.](assets/effurt-asset-edit-handles.png)

#### Implementation

To create EFFURT, we employed a conventional single-page web application (SPA) tool stack, including:

1. **HTML5 and JavaScript**: EFFURT's visual components are written using HTML5, and its logic is written in JavaScript. This allows EFFURT to run within a user's web browser across a wide variety of platforms, including laptops, desktops, and smartphones. 

2. **PouchDB and CouchDB (https://pouchdb.com/)**: To easily distribute and share data between users, EFFURT uses a lightweight JavaScript database called PouchDB which synchronizes with a remotely-hosted CouchDB database.

![Fig. 1.6: View of the EFFURT application where the user is changing between different visible layers.](assets/effurt-layers.png)

3. **Leaflet (https://leafletjs.com/)**: To render and annotate maps, EFFURT uses Leaflet, an open-source HTML5 and JavaScript mapping platform that has been proven to work by multiple other research groups (Bergmann, 2016, South Seeds, 2017, Heinricher, Kahn, Maitland, and Manor, 2013). Leaflet allows EFFURT to display maps as layers (Fig. 1.6) on top of one another; for example, it can display a layer for a satellite map of Kyoto, a layer for a street map of Kyoto, and a separate layer for urban assets in Kyoto.

4. **OpenStreetMap (https://www.openstreetmap.org/)**: By itself, Leaflet does not provide any map tiles (images of an actual two-dimensional map). As a result, EFFURT uses OpenStreetMap, a freely available repository of maps that are constantly updated by communities worldwide, to display map tiles.

5. **Overpass (http://overpass-api.de/)**: OpenStreetMap provides a large amount of data beyond just map tiles—data like urban asset locations. To filter through all of this data and help our team quickly visualize urban assets within a region, EFFURT uses Overpass—a search tool for OpenStreetMap—to rapidly query and display urban assets that we are interested in on maps.

At the time of writing this report, the complete EFFURT source code and implementation was publicly available on GitLab at <https://gitlab.com/crahda/kyoto-b-18-iqp-urban-regeneration>.

## EFFURT Results and Analysis

Looking at the data we gathered in EFFURT, we found that Yokai Street has many anchors and assets. Yokai Street consists of the following:

- 14 Miscellaneous shops including flowers, stationary, bedding, hardware, scents and pet fish
- 12 Food shops with products ranging from vegetables to cookies
- 10 Services including hair salons, clinics, acupuncture, counseling, dry cleaning, printing, mechanic etc.
- 9 Clothing shops ranging from school uniform shops to fashion boutiques
- 9 Restaurants, 3 of which are also bars
- 5 Vending machines
- 4 Cafes
- 4 Closed or abandoned shops
- 3 Shrines and Temples
- 2 Construction Sites
- 2 Traditional Japanese goods shops selling kimonos and/or souvenirs
- 2 Lodging places
- 1 Empty lot
- 1 Bank

The majority of assets of interest to visitors and tourists are situated at the Nishioji end of Yokai Street across the Tenjin river, including Golden Moja Hall, an Izakaya Bar, an Italian restaurant, a fry restaurant, and an Indian Restaurant, highlighted in yellow in figure 1.7. Many of the shops that are open during the day are shops that sell everyday goods and not necessarily points of interests for tourists. A majority of restaurants and bars on Yokai Street do not operate until the evening or later at night. 

The figure differentiates between shops that operate during the day and shops that operate much later into the night. The seven shops bounded in yellow rectangles are shops that open past 19:30, which is the time by which all daytime shops close. (Fig. 1.7) These shops in yellow rectangles are all restaurants and bars that have much more potential to attract tourists than the other shops. (Fig. 1.7) Since Yokai Street only has a few of these bars and restaurants, it is less likely to attract tourists on a large scale, especially young visitors.

![Fig. 1.7: Overview of Yokai Street via EFFURT.](assets/day-night.png)

Upon searching Kamigyo Ward on Google Maps, we discovered that the left bound of the highlighted section for Kamigyo Ward cuts off exactly at the Tenjin river. (Fig. 1.8) Digging deeper into the history of Yokai Street (which we discuss further in our chapter *"The Aura of Yokai Street"*), we discovered that this short section on the Nishioji side of the river which contains many of the bars and resaurants was originally not part of Yokai street and only become an extension years later in order to reach Nishioji Dori. (Fig. 1.9)

![Fig. 1.8: Overview of Kamigyo Ward. (Google Maps, 2018)](assets/kamigyo.png)

This stretch of the street does not feel as integrated into Yokai Street, and seems to be isolated from the rest of the street. (Fig. 1.9) Visitors might not be aware of this new extension and might not think it’s part of Yokai Street and therefore not visit this side of the street as much as they would the other side. (Fig. 1.9) In addition, since this extension connects to Nishioji dori, a major street, visitors may enter Yokai Street from Nishioji dori to patronize bars and restuarants but never cross the Tenjin River into the rest of Yokai Street.

![Fig. 1.9: The short section of Yokai Street by Nishioji-Dori.](assets/short-section-yokai.png)

On Gozen dori, the side street which connects Yokai Street to Kitano Tenmangu Shrine, there is a large museum and a variety of other assets including a traditional udon shop and many restaurants and cafes. These assets, which can be seen circled in black in figure 1.10. only exist on the half of the street closest to Kitano Tenmangu Shrine, before Gozen Dori intersects with Yokai Street. Hence, if tourists were to enter Gozen Dori, from Kitano Tenmangu, they might lose interest and stop walking towards the Yokai Street intersection. Additionally, on Tenjin dori, there is another museum and a liquor store. All of these could be potential points of interests for tourists that exist around Yokai Street. 

![Fig. 1.10: Assets on Yokai Street via EFFURT.](assets/effurt-latest.png)

#### Opportunities and Recommendations for Yokai Street

After looking and analyzing the results we got from EFFURT, we found that Yokai street has a lot of potential and opportunities to improve from. We have listed the recommendations for Yokai Street below.

##### 1: Invest and Develop

Aside from the shops and temples, we also found empty lots and abandoned buildings from EFFURT data. Currently, there are 4 shops that are abandoned on Yokai Street and 3 empty lots, 2 of which are under construction, as seen in the right of the figure 1.11. All of these could be opportunities for both investors and Yokai Street itself to potentially develop more commercial infrastructure and public spaces because the shops on Yokai Street are currently spread out in between a lot of residential buildings and apartments. 

The north section of Yokai street, shown on the left side of figure 1.11 and circled in red in figure 1.10, is underdeveloped as there are currently only a few shops there and also a strip of old residential housing and an abandoned building by the entrance of the street are preventing Yokai street from presenting a welcoming environment for tourists. We also recommend developing and improving the visual appeal of Yokai Street so that it gives off more of a shotengai vibe and becomes much more attractive and appealing to visitors. We have discussed this in much further lengths in our chapter *"Visual Appeal"*.

![Fig. 1.11: Strip of residential buildings and apartments on Yokai Street. (Left) and Construction lot on Yokai Street. (Right)](assets/yokai-street-development.png)

##### 2: More experience assets

Although Yokai Street has a lot of different shops and services, a lot of them are shops that cater to the local community and not necessarily shops that spark tourists’ interests. From EFFURT data, we know that a majority of shops on Yokai Street sell daily necessities, stationary, clothing etc. Tourists, especially young people, are more likely to visit places that offer experiences, such as bars, restaurants etc. As we have mentioned previously, the area of Yokai Street that consists of the majority of these bars and restaurants is on the far end of the street by Nishioji Dori, circled in green in figure 1.10, such as Golden Moja Hall, also shown on the right of figure 1.12.

Also, there are a lot of assets not only on Yokai Street but also on the streets in the nearby area surrounding Yokai Street, such as the liquor store on Tenjin Dori, the Kyoto Butsuryu Museum on Gozen Dori, as shown on the left of figure 1.12. If Yokai Street were to have more shops, events, and activities that offer experiences, it could benefit from these assets and become a destination for young travellers. We have also discussed this in further details in our chapter *"Walking in the Yokai Parade"*.

![Fig. 1.12: Kyoto Butsuryu Museum on Gozen Dori. (Left) and Golden Moja Hall Izakaya on Yokai Street. (Right)](assets/moja.jpg)

##### 3: Promote historical and cultural assets

Looking at EFFURT data, we discovered that there are currently 2 Buddhist temples and a shrine on Yokai Street. Among them, the Daishogun Hachi Jinja Shrine, circled in purple in figure 1.10, has a lot of potential to become a major selling point for Yokai Street considering its rich history and it also has its own museum at which tourists can find out a great deal about the ancient history of Kyoto. We think that Yokai Street could exploit these cultural assets since tourists mostly visit Kyoto to learn and experience its history and culture. We have discussed the recommendation for this in further detail in our chapter *"The Aura of Yokai Street"*. 

##### 4: Promote lodging

Yokai Street could also benefit from promoting the available lodging in the area as there are currently 2 inns on Yokai Street and one on Tenjin Dori. This could be as simple as putting up signs at certain corners of the street in foreign languages pointing at the directions of the inns so that tourists are aware that there is lodging in the area. Although most people find lodging through online resources such as AirBNB and other websites, some tourists still do not necessarily plan ahead and physical directional signs could be easier and more efficient for foreign tourists to navigate because popular navigation apps such as Google Maps are not always accurate, especially in a foreign country like Japan.