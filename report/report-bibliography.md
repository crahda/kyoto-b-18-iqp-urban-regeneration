# References

Amano, T. (2010, June 14). How to promote ‘Cool Japan’?. *The Wall Street Journal*. Retrieved from https://blogs.wsj.com/japanrealtime/2010/06/14/how-to-promote-cool-japan/

Amirtahmasebi, R., Orloff, M., Wahba, S., & Altman, A. (2016). Regenerating Urban Land - A Practitioner's Guide to Leveraging Private Investment. *World Bank Group.*

Balsas, C. (2016). Japanese shopping arcades, pinpointing vivacity amidst obsolescence. *The Town Planning Review*. 87(2), 205-232. DOI: 10.3828/tpr.2016.15

Bergmann, J.V. (2016). Mixing Data. *Mountain Doodles.* Retrieved from https://doodles.mountainmath.ca/blog/2016/07/06/mixing-data/

bluegreen405. (2012). 妖怪ブロンズ像～水木先生と妖怪たち.jpg. *Flickr.* Retrieved from https://www.flickr.com/photos/sunnyday365/6940770975

Boyd, D. J. (2015). Hollowed Out: Exhuming an Ethics of Hollowing in Tite Kubo’s Bleach. *Pacific Coast Philology* 50(2), 242-267. Retrieved from https://muse-jhu-edu.ezproxy.wpi.edu/article/605395

Brumann, C. & Schulz, E. (2012). Shrinking cities and liveability in Japan. In *Urban Spaces in Japan: Cultural and Social Perspectives* (Chapter 11). Retrieved from https://books.google.com/books?id=s5Z-7CYdyN0C&pg=PA208&lpg=PA208&dq=urban%20decay%20in%20japan&source=bl&ots=yqXrZW2M2d&sig=Y1umCC2P2z98p7-pvtcDCJStjoM&hl=en&sa=X&ved=2ahUKEwjz7OfE4qzdAhXDJt8KHSpYBtc4HhDoATACegQIBxAB#v=onepage&q=urban%20decay%20in%20japan&f=false

Cataldo, A. (2018, March 27). Companies try to fit in with millennial's experience economy. *UWIRE Text*. Retrieved from http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A532507576/AONE?u=mlin_c_worpoly&sid=AONE&xid=61140e78

Cremer, S. (2018). The Impact of Scarcity Messages on the Online Sales of Physical Information Goods. *University of Cologne.* Retrieved Decmeber 14th from https://pdfs.semanticscholar.org/6f3b/bd840383f9319e65e927e530a9d729c005de.pdf 

Cruz, C. (2010). Wizarding World of Harry Potter Castle.jpg. *Wikimedia Commons.* Retrieved from https://commons.wikimedia.org/wiki/File:Wizarding_World_of_Harry_Potter_Castle.jpg

DigitalGov. (2013, February 14). *QR codes.* Retrieved from https://digital.gov/2013/02/14/qr-codes/ 

Eastman, E. (n.d.). Pagoda, temple, house, and lake. Retrieved December 11th, 2018, from <https://unsplash.com/photos/wJj5Bs1jHxg>

Foster, H. (n.d.). “Fun Things to do in Kyoto: Hunt Monsters in Yokai Street”. *Destination>Differetville.* Retrieved from https://differentville.com/hunting-monsters-in-kyotos-yokai-street/

Google Maps. (2018). *Google*. Retrieved from https://www.google.com/maps/@35.0264153,135.7363514,125m/data=!3m1!1e3

Hani, Y. (2005). Shotengai. *The Japan Times*. Retrieved from https://www.japantimes.co.jp/life/2005/06/12/to-be-sorted/shotengai

Heinricher, D., Kahn, L., Maitland, I., Manor, N. (2013). *Ecclesiastical Architecture of Venice: Preserving Convents, Churches, Bells and Bell Towers. Worcester Polytechnic Institute.* Retrieved from https://web.wpi.edu/Pubs/E-project/Available/E-project-122013-112739/unrestricted/Venice_B13_Final_IQP_Report.pdf

Hilton, S. (2007). The social value of brands. In *Brands and Branding* (pp.47-64). Retrieved from http://www.culturaldiplomacy.org/academy/pdf/research/books/nation_branding/Brands_And_Branding_-_Rita_Clifton_And_John_Simmons.pdf#page=64 on December 14th, 2018

“Hirano Shrine” (n.d.). *Japan Visitor*. Retrieved from https://www.japanvisitor.com/japan-temples-shrines/hirano-shrine

Hiro - Kokoro☆Photo. (2014). R0017304.jpg. *Flickr*. Retrieved from https://www.flickr.com/photos/kagen33/14662212399/

Hoskin, P. (2015, January 31). How Japan became a pop culture superpower. *The Spectator*. Retrieved from https://www.spectator.co.uk/2015/01/how-japan-became-a-pop-culture-superpower/

Inada, T. (2017). The "Cool Japan" Strategy: Sharing the Unique Culture of Japan with the World[video file]. *Japan Society NYC.* Retrieved from https://www.japansociety.org/webcast/the-cool-japan-strategy-sharing-the-unique-culture-of-japan-with-the-world

Ito, R. (2015, May 24). Pokmon’s Spooky Ancestors. *The New York Times*. Retrieved from http://go.galegroup.com.ezproxy.wpi.edu/ps/i.do?&id=GALE|A414743975&v=2.1&u=mlin_c_worpoly&it=r&p=AONE&sw=w

Japan Guide. (2018). “Gion”. *Japan Guide.* Retrieved from https://www.japan-guide.com/e/e3902.html

Japan Guide. (2018). “Nishiki Market”. *Japan Guide.* Retrieved from https://www.japan-guide.com/e/e3931.html

Joburg City Parks and Zoo. “Three new parks launched in Jan Hofmeyer”. *JCPZ.* Retrieved from http://www.jhbcityparks.com/index.php/news-mainmenu-56/1600-three-new-parks-launched-in-jan-hofmeyer

Kinukake-no-Michi Road Association. (2018). "Kinukake-no-Michi Road Strolling Map." *Kinukake-no-Michi Road Association.* Retrieved from https://kinukake.com/en/osanpomap.html

Kohler, C. (2017, June 24). Millennials jump into experience economy. *The Australian*, p.25 Retrieved from http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A496605858/AONE?u=mlin_c_worpoly&sid=AONE&xid=5080095a

“Kuginuki Jizo Shakuzoji Temple”. (n.d.). *Japan Visitor*. Retrieved from https://www.japanvisitor.com/japan-temples-shrines/kuginuki-jizo-shakuzoji-temple

Kyotohyakki (n.d.). Yokai Tourism in Kyoto. Retrieved from http://www.kyotohyakki.com/tai-syo-gung/tai-syo-gung_e.html

Larke, R. (1992). Japanese Retailing: Fascinating, but Little Understood. *International Journal of Retail & Distribution Management*, 20(1), 3+. Retrieved from http://ezproxy.wpi.edu/login?url=https://search.proquest.com/docview/210922751?accountid=29120

Let’s Live in Kyoto. (2018). Daiei-dori shopping street. *Erics Co.* Retrieved from https://www.elitz.co.jp/kyoto/play/play_detail_04_03.html

Lewis, L. & Jacobs, E. (2018, July 14). How insta-bae are you? Unlike their parents, who strove to accumulate things, Japan's millennials are pursuing Instagram-ready moments. *The Financial Times*, p 18. Retrieved from http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A546448130/AONE?u=mlin_c_worpoly&sid=AONE&xid=819a1441

Life & Culture Information Newsletter. (n.d.). Life In Kyoto. *Life & Culture Information Newsletter*. Retrieved from http://lik.kcif.or.jp/archives/1208/08_2012en.htm

MacIntyre, B., Bolter, J. D., & Gandi, M. (2004). Presence and the Aura of Meaningful Places. Presence, 36-43. Retrieved December 10, 2018, from <http://www.yorku.ca/caitlin/futurecinemas/coursepack2009/aura.pdf>

McGray, D. (2002). Japan's Gross National Cool. *Foreign Policy*, 130, 45-54. Retrieved from http://web.mit.edu/condry/Public/cooljapan/Feb23-2006/McGray-02-GNCool.pdf

McCurry, J. (2018, June 15). 'Tourism pollution': Japanese crackdown costs Airbnb $10m. *The Guardian*. Retrieved from https://www.theguardian.com/world/2018/jun/15/tourism-pollution-backlash-japan-crackdown-costs-airbnb-10m-kyoto

METI. (2014). Vibrant Ganbaru. *METI.* Retrieved from http://www.meti.go.jp/english/policy/sme_chiiki/regional_information/pdf/2014ganbarueng.pdf#page=452

Miyazawa, M., Miyazawa, S., Hokage, N. (n.d.). Hidden Gems of Kyoto Vol 1. 

Mojoprint. (2018). "Taurpaulin Banners." *Mojoprint*. Retrieved from https://mojoprint.jp/products/item/tarpaulin-banners

Murakami & Yoshikura. (2015). Yokai Street. *The Kyoto Project*. Retrieved from http://thekyotoproject.org/english/yokai-street/

Nashima, M. (1997, Aug.). Old-style shopping arcades struggling to survive. *Japan Times*. Retrieved from https://search-proquest-com.ezproxy.wpi.edu/docview/218920603?rfr_id=info%3Axri%2Fsid%3Aprimo

Nissy-KITAQ. (2008). Nishijin chuo shotengai.JPG. *Wikimedia Commons.* Retrieved from https://commons.wikimedia.org/wiki/File:Nishijin_chuo_shotengai.JPG

National Park Service. (2018). *Junior Ranger Programs.* Retrieved from https://www.nps.gov/kids/jrRangers.cfm 

OPEN Glasgow. (2014). Urban Asset Mapping. *OPEN Glasgow.* Retrieved from http://open.glasgow.gov.uk/datastories/urbanassetmapping/

Pine, J. & Gilmore, J. (1998). Welcome to the Experience Economy. *Harvard Business Review.* Retrieved from https://hbr.org/1998/07/welcome-to-the-experience-economy

Pizam, A. (2010). Creating memorable experiences. *International Journal of Hospitality Management*, 29(3), 343. DOI: 10.1016/j.ijhm.2010.04.003

“Promoting Cool Japan”. (2010). *The Japan Times.* Retrieved from https://www.japantimes.co.jp/opinion/2010/08/15/editorials/promoting-cool-japan/#.W-ECaJMzY2w

“Resturant Inoue” (n.d.). *kyoto taishogun yokai street*. Retrieved from http://kyoto-taisyogun.com/en/restaurant-inoue/

Ryall, J. (2017, September 9). ‘Pollution by Tourism’: How Japan fell out of love with Visitors from China and beyond. *South China Morning Post*. Retrieved from 
https://www.scmp.com/week-asia/society/article/2110388/pollution-tourism-how-japan-fell-out-love-visitors-china-and

Ryall, J. (2018, September 11). Japan tourism: The polite Japanese are finally getting sick of tourists. *Traveller*. Retrieved from
http://www.traveller.com.au/japan-tourism-the-polite-japanese-are-finally-getting-sick-of-tourists-h157ik

Shamoon, D. (2013). The Yōkai in the Database: Supernatural Creatures and Folklore in Manga and Anime. *Marvels & Tales* 27(2), 276-289. Retrieved from https://muse-jhu-edu.ezproxy.wpi.edu/article/524101

“Shonenji Temple (Neko or Cat Temple)”. (n.d.). *Shounen-Temple*. Retrieved from http://nekodera.net/html/english.html

Social Value Lab. (2012). Maybole Community Assets Mapping Study. *Social Value Lab.* Retrieved from http://www.socialvaluelab.org.uk/wp-content/uploads/2013/05/Maybole-Community-Assets-Final-report.pdf

Sorasak. (2017). Two women in purple and pink kimono standing on street. *Unsplash.* Retrieved from https://unsplash.com/photos/_UIN-pFfJ7c

South Seeds. (2017). Development of a community asset on Glasgow’s southside. *South Seeds.* Retrieved from http://southseeds.org/wp-content/uploads/2017/09/Developing-a-community-asset-on-Glasgows-southside.pdf

The Invisible Tourist. (2018, June 25). Overtourism: Is Japan Becoming a Victim of its Own Success? *The Invisible Tourist*. Retrieved from https://www.theinvisibletourist.com/overtourism-in-japan-tourist-pollution/

The Japan Times. (2016, May 7). Find an effective tourism strategy. *The Japan Times*. Retrieved from https://www.japantimes.co.jp/opinion/2016/05/07/editorials/find-effective-tourism-strategy/#.W9-me5Mzbb1

The Kyoto Shimbun. (2018, June 30). Concerns in Kyoto over Tourism Pollution. *The Kyoto Shimbun*. Retrieved from https://e.kyoto-np.jp/news/20180630/3220.html

Toshiyuki, I. (2007). Nishiki Ichiba.jpg. *Wikimedia Commons.* Retrieved from https://ja.m.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:Nishiki_Ichiba_by_matsuyuki.jpg

Treist, P. (2017) Build an Interactive Game Of Thrones Map (Part II) - Leaflet.js & WebPack. *Patrick Triest.* Retrieved from https://blog.patricktriest.com/game-of-thrones-leaflet-webpack

The Workers of Daishogun Hachijinja Shrine

Yano, K. (2016). Overlaying Maps of Modern Kyoto. Retrieved December 10th, 2018, from <http://www.arc.ritsumei.ac.jp/archive01/theater/html/ModernKyoto/> 

Yanase, M. (n.d.). Pavement, sidewalk, concrete, and rock garden. Retrieved December 12th, 2018, from https://unsplash.com/photos/peAbdH4O8GM