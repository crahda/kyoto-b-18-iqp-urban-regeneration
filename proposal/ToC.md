```
1. Introduction (p. 2; Sanders, B.)

2. Background (p. 2)

  2.1 Case Studies of Urban Decay and Regeneration (p. 2; Lee, J.)
    2.1.1 Case Study: Seoul (pp. 2-3; Lee, J.)
    2.1.2 Case Study: Santiago (p. 3; Lee, J.)
    2.1.3 Case Study: Buenos Aires (pp. 3-4; Lee, J.)
    2.1.4 Case Study: Johannesburg (pp. 4-5; Lee, J.)
    2.1.5 Summary of Case Studies (pp. 5-6; Lee, J.)
        
  2.2 Urban Decay and Regeneration in Japan (pp. 6-7; Linn-Thant, S.)
    2.2.1 Spirituality and Religion (pp. 7-8; Linn-Thant, S.)
    2.2.2 Short Lifespans of Japanese Houses (pp. 8-9; Linn-Thant, S.)
    2.2.3 Economic Growth and the "Lost Decade" in Japan (pp. 9-10; Hunker, O.)
    2.2.4 Transformation of shopping culture and the Decay of Shotengai (pp. 10-11; Hunker, O.)
        2.2.4.1 Shotengai and Changes in Consumer Trends (pp. 10-11; Hunker, O.) 
        2.2.4.2 Shotengai, Communities, and Culture (p. 11-12; Hunker, O.)
    2.2.5 Japanese Case Studies (p. 12; Hunker, O.)
        2.2.5.1 Shopping Districts in Izumisano, Osaka Perfecture (pp. 12-13; Hunker, O.)
        2.2.5.2 The Ichijo-Dori Shotengai and Yokai Street (pp. 13-14; Hunker, O.)
        
  2.3 Mapping for Analytics (pp. 14; Sanders, B.)
    2.3.1 Example Applications of Mapping for Analytics (pp. 14; Sanders, B.)
      2.3.1.1 Charles Booth's Poverty Maps (p. 14; Sanders, B.)
      2.3.1.2 An Abstract Map (p. 14-15; Sanders, B.)
      2.3.1.3 An Interactive Map (pp. 15; Sanders, B.)
      2.3.1.4 A Mixed Data Map (p. 15; Sanders, B.)
    2.3.2 Urban Asset Mapping (pp. 15-16; Sanders, B.)
    2.3.3 Creating Interactive and Mixed Data Maps (pp. 16-17; Sanders, B.)

3. Proposed Methodology (p. 17-18; Sanders, B.)

  3.1 The Electronic Field Form for Urban ReviTalization (p. 18; Sanders, B.)
    3.1.1 Proposed Features (pp. 18-19; Sanders, B.)
    3.1.2 Proposed Method (p. 19-; Sanders, B.)

  3.2 Regional Survey using Open Data (pp. 19-20; Hunker, O.)
    3.2.1 GIS Data (pp. 20-21; Hunker, O.)
    3.2.2 Census Data (pp. 21-23; Hunker, O.)
    3.2.3 Events Data (p. 23; Hunker, O.)

  3.3 Using Urban Ley-lines to Categorize Shōtengai (pp. 23-24; Sanders, B.)
    3.3.1 Proposed Method (pp. 24; Sanders, B.)

  3.4 Local Survey using EFFURT (p. 24-25: Lee, J.)
    3.4.1 Proposed Method (p. 25; Lee, J.)
        3.4.1.1 Decay Factors (pp. 25; Lee, J.)

  3.5 Tourism Map (pp. 25-26; Linn-Thant, S.)

  3.6 Decay Map (pp. 26-27; Linn-Thant, S.)
    
Bibliography (p. 27-31)
```