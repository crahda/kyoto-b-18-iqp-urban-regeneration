// This code will not execute within the global scope.
(function() {

    // Positron base map layer.
    var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
	    attribution: 'Tiles &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="https://carto.com/attributions">CARTO</a>',
	    subdomains: 'abcd',
	    maxNativeZoom: 19,
	    maxZoom: 22
    });

    // ESRI Topographic map layer.
    var Esri_WorldTopoMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
    	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community',
        maxNativeZoom: 19,
    	maxZoom: 22
    });

    // ESRI Satellite map layer.
    var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
        maxNativeZoom: 17,
    	maxZoom: 22
    });

    // Collection of urban asset markers currently drawn on the map.
    var urbanAssetMarkerFeatures = new L.FeatureGroup();

    // Collection of overpass markers currently drawn on the map.
    var overpassMarkerFeatures = new L.FeatureGroup();

    // Create a new Leaflet map centered on the Taishogun-Shotengai (Yokai Street).
    map = L.map("map", {
        center: [35.02704444650624, 135.7570266723633], // Originally [35.026197, 135.73595],
        zoom: 14, // Originally 18
        layers: [Esri_WorldTopoMap, urbanAssetMarkerFeatures, overpassMarkerFeatures]
    });

    // List of base map layers (tiles).
    var baseMaps = {
        "Base": CartoDB_Positron,
        "Topo": Esri_WorldTopoMap,
        "Satellite": Esri_WorldImagery
    };

    // List of overlay map layers (markers and features).
    var overlayMaps = {
        "Urban Assets": urbanAssetMarkerFeatures,
        "Overpass Queries": overpassMarkerFeatures
    };

    // Add a control for all map layers to the map.
    L.control.layers(baseMaps, overlayMaps).addTo(map);

    // Utility for adding an asset popup marker to a map.
    function addAssetPolygon(layer, assetMarker, map) {

        var id = assetMarker._id;

        // If the asset is already on the map, remove it
        // so we do not get duplicate assets on the map.
        if (urbanAssetMarkers[id]) {
            map.removeLayer(urbanAssetMarkers[id]);
            urbanAssetMarkers[id] = null;
        }

        // Add a clicky handler.
        layer.on("click", function(event) {
            urbanAssetDb.get(id, {attachments: true})
            .then(function (doc) {

                // Load and display asset.
                assetMarkerModal.asset = new UrbanAsset(doc);
                assetMarkerModal.image = "https://via.placeholder.com/640x480.png?text=No%20Image";
                assetMarkerModal.newImage = null;
                assetMarkerModal.editMode = mapEditing;
                $("#asset-marker-modal").modal("show");

                // Load image if present.
                if (doc._attachments && doc._attachments.image != null) {
                    urbanAssetDb.getAttachment(id, "image")
                        .then(function (blob) {
                            assetMarkerModal.image = URL.createObjectURL(blob);
                        })
                        .catch(function (err) {
                            console.log(err);
                        });
                }
            });
        });

        urbanAssetMarkers[id] = layer;
        layer.urbanAssetId = id;
    }

    // Utility for adding all asset markers from the local database to the map.
    function addAssetPolygonsFromLocalDb(map) {
        return urbanAssetDb.allDocs({include_docs: true})
            .then(function (docs) {

                // Clear existing assets from the map.
                urbanAssetMarkerFeatures.eachLayer(function(layer) {
                    urbanAssetMarkers[layer.urbanAssetId] = null;
                    map.removeLayer(layer);
                });

                // Iterate and add all local assets.
                for (var asset in docs.rows) {

                    // Get the asset.
                    var asset = docs.rows[asset].doc;

                    // Create polygon.
                    var layer = L.polygon(asset.ll);
                    layer.addTo(urbanAssetMarkerFeatures);

                    // Register polygon handlers.
                    addAssetPolygon(layer, asset, map);
                }
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    // Setup map logo layer.
    var logo = L.control({position: 'bottomleft'});
    logo.onAdd = function(map) {
        var div = L.DomUtil.create('div', 'wpi-logo-layer');
        div.innerHTML = "<img src='assets/wpi-logo-red.png'/>";
        return div;
    };
    logo.addTo(map);

    // Setup drawing controls.
    var drawControl = new L.Control.Draw({
        draw: {
            marker: false,
            rectangle: false,
            circle: false,
            polyline: false,
            marker: false,
            circlemarker: false
        },
        edit: {
            featureGroup: urbanAssetMarkerFeatures,
            remove: false,
            edit: {
              moveMarkers: false // centroids, default: false
            }
        }
    });
    map.addControl(drawControl);

    // Capture edit start and stop events.
    map.on(L.Draw.Event.EDITSTART, function(e) {
        mapEditing = true;
    });
    map.on(L.Draw.Event.EDITSTOP, function(e) {
        mapEditing = false;
    });

    // Setup database controls.
    var databaseControl = new L.Control({position: 'topright'});
    databaseControl.onAdd = function(map) {

        // Create a div with a database logo.
        var div = L.DomUtil.create('div', 'database-control-layer');
        div.innerHTML = "<button type=\"button\" class=\"btn btn-secondary btn-md\"><span class=\"fas fa-sync-alt fa-lg\" aria-hidden=\"true\"></span></button>";

        // Flag to ensure only one event processor is running at once.
        var synchronizing = false;

        // Register a handler to synchronize the database and show
        // previously loaded assets.
        L.DomEvent.on(div, 'click', function(event) {

            // Prevent this event from propogating further.
            L.DomEvent.stopPropagation(event);

            // If a sync is in progress, exit.
            if (synchronizing) {
                return;
            }

            // We are now synchronizing.
            synchronizing = true;

            // Disable the clicked div to prevent further interaction.
            // The div represents a div containing the button, so we
            // must use the firstChild of the div (the button).
            L.DomUtil.addClass(div.firstChild, 'disabled');

            // Add the spin class to the icon inside the button div
            // to show that a sync is in progress.
            L.DomUtil.addClass(div.firstChild.firstChild, 'fa-spin');

            // Begin synchronizing the database.
            var urbanAssetDbStabilized = new Promise(function(resolve, reject) {

                // Compress all image assets first...
                compressUrbanAssetDbImages().then(function() {
                    urbanAssetDb.sync(urbanAssetDbRemote, {
                        live: false,
                        retry: true
                    }).on("complete", function() {
                        console.log("Synchronization completed. Resolving.");
                        resolve();
                    }).on("error", function(err) {
                        console.err("PouchDB Synchronization Error: " + err);
                        resolve(err);
                    }).on('change', function (info) {
                      // handle change
                      console.log("Replicating: " + info);
                    }).on('paused', function (err) {
                      // replication paused (e.g. replication up to date, user went offline)
                      console.log("Replication paused...");
                    }).on('active', function () {
                      // replicate resumed (e.g. new changes replicating, user went back online)
                      console.log("Replication resuming...");
                    }).on('denied', function (err) {
                      // a document failed to replicate (e.g. due to permissions)
                      console.log(err);
                    });
                });
            });

            // Load all assets from the local database.
            urbanAssetDbStabilized.then(function() {
                addAssetPolygonsFromLocalDb(map)
                    .then(function() {
                        // Enable the clicked div to allow further interaction.
                        L.DomUtil.removeClass(div.firstChild, 'disabled');
                        L.DomUtil.removeClass(div.firstChild.firstChild, 'fa-spin');

                        // We are done synchronizing.
                        synchronizing = false;
                    });
            });
        });

        // Return the div.
        return div;
    };
    map.addControl(databaseControl);

    // Setup location control.
    var locationControl = new L.Control({position: 'topright'});
    locationControl.onAdd = function(map) {

        // Create a div with a location icon.
        var div = L.DomUtil.create('div', 'location-control-layer');
        div.innerHTML = "<button type=\"button\" class=\"btn btn-secondary btn-md\"><span class=\"fas fa-location-arrow fa-lg\" aria-hidden=\"true\"></span></button>";

        // Register a handler 
        L.DomEvent.on(div, 'click', function(event) {

            // Prevent this event from propogating further.
            L.DomEvent.stopPropagation(event);

            // Get current location and pan to it.
            navigator.geolocation.getCurrentPosition(function(position) {
                map.setView([position.coords.latitude, position.coords.longitude]);
                console.log(position);
            },

            function(error) {
                console.err(error);
            },

            {
                maximumAge: 600000, 
                timeout: 5000, 
                enableHighAccuracy: true
            });
        });

        //Return the div.
        return div;
    }
    map.addControl(locationControl);

    // Setup overpass query control.
    // TODO: UPDATE MEEEE
    var overpassQueryControl = new L.Control({position: 'topright'});
    overpassQueryControl.onAdd = function(map) {

       // Create a div with a location icon.
       var div = L.DomUtil.create('div', 'overpass-query-control-layer');
       div.innerHTML = "<button type=\"button\" class=\"btn btn-secondary btn-md\"><span class=\"fas fa-search fa-lg\" aria-hidden=\"true\"></span></button>";

       // Register a handler
       L.DomEvent.on(div, 'click', function(event) {

            // Prevent this event from propogating further.
            L.DomEvent.stopPropagation(event);

            // Filter string to match assets against.
            var filterString = "priv";

            // Iterate over all urban asset markers.
            urbanAssetMarkerFeatures.eachLayer(function(layer) {

                // Get the list of assets for the marker.
                urbanAssetDb.get(layer.urbanAssetId, {attachments: true})
                .then(function (doc) {
                    var assets = doc.assets;

                    // Search for the filter string in the marker's assets.
                    var matches = false;
                    for (var i = 0; i < assets.length; i++) {
                        if (assets[i].includes(filterString)) {
                            matches = true;
                            break;
                        }
                    }

                    // Set each marker's visibility based on whether
                    // or not it partially (or fully) matches the
                    // filter string.
                    layer.getElement().style.display = matches ? "inline" : "none";
                });
            });

//           var queryTextFieldValue = "\"amenity\"=\"cafe\"";
//           var overpassApiUrl = buildOverpassApiUrl(map, queryTextFieldValue);

//           $.get(overpassApiUrl, function (osmDataAsJson) {
//               var resultAsGeojson = osmtogeojson(osmDataAsJson);
//               var resultLayer = L.geoJson(resultAsGeojson, {
//                   style: function (feature) {
//                       return {
//                           color: "#ff0000"
//                       };
//                   },
//                   filter: function (feature, layer) {
//                       var isPolygon = (feature.geometry) && (feature.geometry.type !== undefined) && (feature.geometry.type === "Polygon");
//                       if (isPolygon) {
//                           feature.geometry.type = "Point";
//                           var polygonCenter = L.latLngBounds(feature.geometry.coordinates[0]).getCenter();
//                           feature.geometry.coordinates = [polygonCenter.lat, polygonCenter.lng];
//                       }
//                       return true;
//                   },
//                   onEachFeature: function (feature, layer) {
//                       var popupContent = "";
//                       //popupContent = popupContent + "<dt>@id</dt><dd>" + feature.properties.type + "/" + feature.properties.id + "</dd>";
//                       var keys = Object.keys(feature.properties.tags);
//                       keys.forEach(function (key) {
//                           if (key.includes("name") || key.includes("internet")) {
//                                popupContent = popupContent + "<dt>" + key + "</dt><dd>" + feature.properties.tags[key] + "</dd>";
//                           }
//                       });
//                       layer.bindPopup(popupContent);
//                   }
//               }).addTo(overpassMarkerFeatures);
//           });
       });

       //Return the div.
       return div;
    }
    map.addControl(overpassQueryControl);

    // Handle the creation of new drawings.
    map.on(L.Draw.Event.CREATED, function(event) {

        // Create polygon.
        event.layer.addTo(urbanAssetMarkerFeatures);

        // Create a new urban asset at the location.
        var urbanAsset = new UrbanAsset();
        urbanAsset.ll = cleanLeafletCoordinates(event.layer.editing.latlngs[0]);
        urbanAssetDb.put(urbanAsset);

        // Register polygon handlers.
        addAssetPolygon(event.layer, urbanAsset, map);

        // Show the modal view for the asset.
        assetMarkerModal.asset = urbanAsset;
        assetMarkerModal.image = "https://via.placeholder.com/640x280.png?text=No%20Image";
        assetMarkerModal.newImage = null;
        assetMarkerModal.editMode = true;
        $("#asset-marker-modal").modal("show");
    });

    // Handle the editing of existing drawings.
    map.on(L.Draw.Event.EDITED, function(event) {
        event.layers.eachLayer(function(layer) {
            if (layer.urbanAssetId) {
                urbanAssetDb.get(layer.urbanAssetId)
                    .then(function (doc) {
                        doc.ll = cleanLeafletCoordinates(layer.editing.latlngs[0]);
                        return urbanAssetDb.put(doc);
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            }
        });
    });

    // Load local polygons.
    addAssetPolygonsFromLocalDb(map);
})();