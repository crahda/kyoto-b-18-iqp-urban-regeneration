// Urban asset database credentials.
// TODO: Note that these credentials are in version control, meaning
//       if the production database uses these credentials, anyone will
//       be able to access the database. This is a terrible practice, but
//       it's good enough for academic work. *wink*
const couchDbHost = "https://effurt-data.alicorn.io:6984";
const couchDbUser = "admin";
const couchDbPassword = "unicorn-narwhal-horse-dragon-bison";

// Urban asset database (local).
const urbanAssetDb = new PouchDB("urban-assets");

// Urban asset database (remote).
const urbanAssetDbRemote = new PouchDB(couchDbHost + "/urban-assets", {
    skip_setup: false,
    auth: {
        username: couchDbUser,
        password: couchDbPassword
    } 
});

// Maximum image size in database.
const maxImageWidth = 1280;
const maxImageHeight = 720;

// Array containing Urban Asset markers we have created or will create.
const urbanAssetMarkers = {};

// The map.
var map;

// True if the map is currently in editing mode, and false otherwise.
var mapEditing;

// Utility for performing batch compression of local database images.
function compressUrbanAssetDbImages() {
    return new Promise(function(resolve, reject) {
        urbanAssetDb.allDocs().then(function (docs) {

            // Track number of remaining documents.
            var remainingDocs = docs.rows.length;

            // Execute foreach.
            docs.rows.forEach(function(asset) {
                var id = asset.id;
                urbanAssetDb.getAttachment(id, "image")
                .then(function (blob) {
                    compressImage(blob, maxImageWidth, maxImageHeight)
                    .then(function (compressedBlob) {
                        console.log("Compression complete for: " + id);
                        urbanAssetDb.get(id, {attachments: true})
                        .then(function (doc) {

                            console.log("Storing compressed image for: " + id);

                            doc._attachments = doc._attachments || {};
                            doc._attachments.image = {
                                data: compressedBlob,
                                content_type: compressedBlob.type
                            };

                            return urbanAssetDb.put(doc);
                        })
                        .then(function () {
                            console.log("Stored compressed image for: " + id);
                        })
                        .catch(function (err) {
                            console.log(err);
                        })
                        .finally(function () {

                            // Decrement the number of remaining documents.
                            remainingDocs--;

                            // If the number of remaining documents is zero or less,
                            // resolve the promise.
                            if (remainingDocs <= 0) {
                                resolve();
                            }
                        });
                    });
                })
                .catch(function (err) {
                    console.log(err);

                    // Decrement the number of remaining documents.
                    remainingDocs--;

                    // If the number of remaining documents is zero or less,
                    // resolve the promise.
                    if (remainingDocs <= 0) {
                        resolve();
                    }
                });
            });
        });
    });
}