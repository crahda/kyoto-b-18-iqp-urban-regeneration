// State of the modal which will be bound to #asset-marker-modal.
var assetMarkerModal = {
    editMode: false,
    saving: false,
    asset: null,
    image: null,
    newImage: null,
    upload: function() {
        var self = assetMarkerModal;
        self.image = URL.createObjectURL($("#asset-marker-image")[0].files[0]);
    },
    close: function() {
        $("#asset-marker-modal").modal("hide");
        assetMarkerModal.saving = false;
    },
    save: function () {
        if (!assetMarkerModal.saving) {
            assetMarkerModal.saving = true;
            var self = assetMarkerModal;
            self.asset.save().then(function() {
                self.close();
            });
        }
    },
    remove: function () {
        var self = assetMarkerModal;
        self.asset.remove();
        $("#asset-marker-modal").modal("hide");
    }
};

// Bind state to the view modal.
rivets.bind($('#asset-marker-modal'), {
    modal: assetMarkerModal
});